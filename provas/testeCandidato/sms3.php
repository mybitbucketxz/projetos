<?php

$body = '{
    "from": "CHAG",
    "to": [
        "+554891876761"
    ],
    "text": "Ok! Enviado"
}';

$string = 'teste_recrutamento:jH09stM9';

$codificada = base64_encode($string);

$headr = array();
$headr[] = 'Content-type: application/json';
$headr[] = 'Authorization: Basic '.$codificada;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.infobip.com/sms/1/text/single");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

$result = curl_exec($ch);

if($result === false) {
    echo "Erro!";
} else {
    echo "Enviado!";

}

?>

