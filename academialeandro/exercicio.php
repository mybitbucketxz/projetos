<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of exercicio
 *
 * @author Willian
 */
class exercicio {
    //put your code here
    
    private $idExercicio,$num,$descricao,$tipoExercicio,$serie,$carga,$idAluno;
    
    
    public function __construct($num,$descricao,$tipoExercicio,$serie,$carga,$idAluno) {
        $this->num = $num;
        $this->descricao = $descricao;
        $this->tipoExercicio = $tipoExercicio;
        $this->serie = $serie;
        $this->carga = $carga;
        $this->idAluno = $idAluno;
    }
    
    
    function getIdExercicio() {
        return $this->idExercicio;
    }

    function setIdExercicio($idExercicio) {
        $this->idExercicio = $idExercicio;
    }

        
    function getIdAluno() {
        return $this->idAluno;
    }

    function setIdAluno($idAluno) {
        $this->idAluno = $idAluno;
    }

        function getNum() {
        return $this->num;
    }

    function getDescricao() {
        return $this->descricao;
    }
    
    function getTipoExercicio() {
        return $this->tipoExercicio;
    }
      

    function getSerie() {
        return $this->serie;
    }

    function getCarga() {
        return $this->carga;
    }

    function setNum($num) {
        $this->num = $num;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }
    
    
    function setTipoExercicio($tipoExercicio) {
        $this->tipoExercicio = $tipoExercicio;
    }

    function setSerie($serie) {
        $this->serie = $serie;
    }

    function setCarga($carga) {
        $this->carga = $carga;
    }


    
    
}
