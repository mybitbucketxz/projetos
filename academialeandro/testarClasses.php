<?php


header("Content-Type: text/html; charset=UTF-8",true); // PARA NÃO APARECEREM CARACTERES ESTRANHOS!

include_once 'aluno.php';
include_once 'exercicio.php';
include_once 'administrador.php';
include_once 'conexao/dataBase.php';

$dataBase = new database();


//$aluno1 = new aluno("joaoamarelologin","senhatal","João Lima Soares", "13/02/2016", "14/03/2016", "10/01/2016", "20","Treinar cada vez mais", "Possui problemas cardiacos", "A");


// Note que o aluno tem o atributo idAluno mas não é necessario colocar no insert, pois o id é auto increment.
//o id do aluno servira para um select ou update.

//PARA CRIAR A QUERY DE INSERÇÃO DE UM ALUNO:
//$inserirAluno1 = "INSERT INTO aluno (login,senha,nome,dataAvaliacao,dataReavaliacao,inicio,idade,objetivos,observacoes,situacao) VALUES (

//'".$aluno1->getLogin()."',
//'".$aluno1->getSenha()."',
//'".$aluno1->getNome()."',
//'".$aluno1->getDataAvaliacao()."',
//'".$aluno1->getDataReavaliacao()."',
//'".$aluno1->getInicio()."',
//'".$aluno1->getIdade()."',
//'".$aluno1->getObjetivos()."',
//'".$aluno1->getObservacoes()."',
//'".$aluno1->getSituacao()."'



 //);";

//PARA EXECUTAR O SQL DE INSERÇÃO DE UM ALUNO:
//$dataBase->insertDB($inserirAluno1);



//$exercicio1 = new exercicio("10", "Pack Jack","Peito","(5x5)", "20",1);
//$exercicio2 = new exercicio("10", "Supino Inclinado","Peito", "(5x5)", "20",1);
//$exercicio3 = new exercicio("10", "Remada Alta","Costas", "(3x5)", "20",1);
//$exercicio4 = new exercicio("10", "CrossOver","Costas", "(2x5)", "20",1);
//$exercicio5 = new exercicio("10", "Abdominal com 1kg","Abdominal", "(15x5)", "20",1);
       
       
// PARA CRIAR A QUERY DE INSERÇÃO DE UM NOVO EXERCICIO:
//$exercicioquery1 = "INSERT INTO exercicio (num,descricao,tipoExercicio,serie,carga,alunoIdAluno) VALUES ('".$exercicio1->getNum()."','".$exercicio1->getDescricao()."','".$exercicio1->getTipoExercicio()."','".$exercicio1->getSerie()."','".$exercicio1->getCarga()."',".$exercicio1->getIdAluno().");";
//$exercicioquery2 = "INSERT INTO exercicio (num,descricao,tipoExercicio,serie,carga,alunoIdAluno) VALUES ('".$exercicio2->getNum()."','".$exercicio2->getDescricao()."','".$exercicio2->getTipoExercicio()."','".$exercicio2->getSerie()."','".$exercicio2->getCarga()."',".$exercicio2->getIdAluno().");";
//$exercicioquery3 = "INSERT INTO exercicio (num,descricao,tipoExercicio,serie,carga,alunoIdAluno) VALUES ('".$exercicio3->getNum()."','".$exercicio3->getDescricao()."','".$exercicio3->getTipoExercicio()."','".$exercicio3->getSerie()."','".$exercicio3->getCarga()."',".$exercicio3->getIdAluno().");";
//$exercicioquery4 = "INSERT INTO exercicio (num,descricao,tipoExercicio,serie,carga,alunoIdAluno) VALUES ('".$exercicio4->getNum()."','".$exercicio4->getDescricao()."','".$exercicio4->getTipoExercicio()."','".$exercicio4->getSerie()."','".$exercicio4->getCarga()."',".$exercicio4->getIdAluno().");";
//$exercicioquery5 = "INSERT INTO exercicio (num,descricao,tipoExercicio,serie,carga,alunoIdAluno) VALUES ('".$exercicio5->getNum()."','".$exercicio5->getDescricao()."','".$exercicio5->getTipoExercicio()."','".$exercicio5->getSerie()."','".$exercicio5->getCarga()."',".$exercicio5->getIdAluno().");";

//PARA EXECUTA O SQL DE INSERÇÃO DE UM NOVO EXERCICIO:
//$dataBase->insertDB($exercicioquery1);
//$dataBase->insertDB($exercicioquery2);
//$dataBase->insertDB($exercicioquery3);
//$dataBase->insertDB($exercicioquery4);
//$dataBase->insertDB($exercicioquery5);



//PARA CRIAR A QUERY DE UPDATE DE UM ALUNO:
$updatealuno1 = "UPDATE aluno set nome = 'willian x' WHERE idAluno=23 "; 

//PARA EXECUTAR O SQL DE UPDATE DE UM ALUNO:
$dataBase->updateDB($updatealuno1);

//PARA CRIAR A QUERY DE DELETE DE UM ALUNO:
//$deletealuno1 = "DELETE FROM aluno WHERE idAluno=1;";

//PARA EXECUTAR O SQL DE DELETE DE UM ALUNO:
//$dataBase->deleteDB($deletealuno1);
//OBS: AO DELETAR UM ALUNO TODOS OS EXERCICIOS VINCULADO A ELE SERAM DELETADOS!




        
$sqlalunos = "SELECT * FROM aluno;";
$selecionarAlunos =  $dataBase->selectDB($sqlalunos);



echo '<h2>Lista de alunos:</h2>';
while($linhasAlunos = mysqli_fetch_assoc($selecionarAlunos)){
  echo '<h3>Informações:</h3>';
  echo 'ID ALUNO: '.$linhasAlunos['idAluno'].'<br>';
  echo 'LOGIN: '.$linhasAlunos['login'].'<br>';
  echo 'SENHA: '.$linhasAlunos['senha'].'<br>';
  echo 'NOME: '.$linhasAlunos['nome'].'<br>';
  echo 'DATA DE AVALIAÇÃO: '.$linhasAlunos['dataAvaliacao'].'<br>';
  echo 'DATA DE REAVALIAÇÃO: '.$linhasAlunos['dataReavaliacao'].'<br>';
  echo 'INICIO: '.$linhasAlunos['inicio'].'<br>';
  echo 'IDADE: '.$linhasAlunos['idade'].'<br>';
  echo 'OBJETIVOS: '.$linhasAlunos['objetivos'].'<br>';
  echo 'OBSERVAÇÕES: '.$linhasAlunos['observacoes'].'<br>';
  echo 'SITUAÇÃO: '.$linhasAlunos['situacao'].'<br>';
  echo '<hr/>';
  
  
  $sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=".$linhasAlunos['idAluno'].";";
  //$sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=2;";
  $selecionarExercicios =  $dataBase->selectDB($sqlExercicios);
  
  echo '<h3>Exercicios Praticados:</h3>'; 
  while($linhasExercicios = mysqli_fetch_assoc($selecionarExercicios)){
        
        echo 'NUMERO: '.$linhasExercicios['num'].'<br>';
        echo 'DESCRIÇÃO: '.$linhasExercicios['descricao'].'<br>';
        echo 'TIPO DO EXERCICIO: '.$linhasExercicios['tipoExercicio'].'<br>';
        echo 'SÉRIE: '.$linhasExercicios['serie'].'<br>';
        echo 'CARGA: '.$linhasExercicios['carga'].'<br>';
        echo '<hr/>';
  }
  
  
} // Fim da consulta dos alunos   


$administrador1 = new administrador("willian", "12345"); // Essas seram o login/senha de 1 administrador.
$administrador2 = new administrador("juliana", "12345"); 
$administrador3 = new administrador("joaoamarelologin", "senhatal"); 
$administrador4 = new administrador("sandro", "12345"); 
$administrador5 = new administrador("paulo", "12345"); 

//Coloquemos todo os nossos administradores dentro de um array.
$arrayAdministradores[] = $administrador1;
$arrayAdministradores[] = $administrador2;
$arrayAdministradores[] = $administrador3;
$arrayAdministradores[] = $administrador4;
$arrayAdministradores[] = $administrador5;


echo 'Login:'.$administrador1->getLogin().'<br>'; // Aqui mostramos o login/senha do primeiro administrador nosso.
echo 'Senha:'.$administrador1->getSenha();


//Simulação do que vai vim da tela de login:
$veiodoformulariologin = 'joaoamarelologin';  //Coloque o usuario e a senha diferente para ver se continua dando login e senha corretos.
$veiodoformulariosenha = 'senhatal';

$senhacriptografada = md5($veiodoformulariosenha."indiscriptografavel"); // Aqui criptografamos para poder comparar as duas senhas.

echo '<hr/>';

$loginCorreto = false; // Já colocamos o login e senha como errados.    
for($i = 0;$i < 5;$i++){ // Percorremos os nossos administradores que estã odentro do array, vericando login/senha.
    if($veiodoformulariologin == $arrayAdministradores[$i]->getLogin() && $senhacriptografada == $arrayAdministradores[$i]->getSenha()){
        $loginCorreto = true; // Então se estiver certo colocamos que o login é verdadeiro.
    }
}

if($loginCorreto == true){
    echo 'O login e Senha está correto, pode prosseguir!';
}else{
    echo 'Login ou Senha errado!';
}

?>