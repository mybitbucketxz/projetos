<?php

include_once 'exercicio.php';

session_start();

if(!isset($_SESSION['admin']) && ($_SESSION['admin'] != true)){
      header("location: Index.html");
      die();
}


if(!isset($_SESSION['exercicio']) && ($_SESSION['exercicio'] != true)){
      header("location: Index.html");
      die();
}




  if($_GET['msg'] == 1){
   echo "<h3 style='color: green;'>ALUNO(a) CADASTRADO COM SUCESSO!</h3>";
   
  }
  
  if($_GET['msg'] == 2){
   echo '<h3>EXERCICIO CADASTRADO COM SUCESSO!</h3>';
   
  }
  
  if($_GET['msg'] == 4){
   echo "<h3 style='color: green;'>Exercicios cadastrados com sucesso!</h3><hr/>";
   echo "<h4 style='color: green;'>Preencha novamente se quiser cadastrar mais exercicios para este aluno!</h4><hr/>";
  }
  
  
?>



<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Exercicios</title>
     <link href="css/estilo.css" rel="stylesheet">
     <link href="css/bootstrap.css" rel="stylesheet">
     <style>
          div{
                margin: 10px;
            }
            
            h1{
                color: black;
            }
            
            h3{
                color: black;
                margin: 20px;
            }
            
            h4{
                color: black;
                margin: 20px;
            }
            
            body{
                
                margin: 20px;
            }
     </style>
    <script src="js/bootstrap.js">    </script>
</head>
    <body>
        
        
        
        
        <form class="form-horizontal" action="cadastrarExercicio.php" method="POST">
            <fieldset id="formulario">

            <!-- Form Name -->
            <legend><h1>Exercícios</h1></legend>

            <!-- Exercicio Costas -->
            <div class="form-group">
                <h3>Costas</h3>
                <div class="col-md-4">
                    <tbody>
                    <tr>
                        <td><input class="form-control" id="tNum" name="cNum" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                        <td><input class="form-control" id="tDesc" name="cDesc" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                        <td><input class="form-control" id="tSerieE" name="cSerieE" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                        <td><input class="form-control" id="tCarga" name="cCarga" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                        
                    </tr>
                    </tbody>
                </div>
            </div>

            <!-- Exercicio Bicepcs -->
            <div class="form-group">
                <h3>Biceps</h3>
                <div class="col-md-4">
                    <tbody>
                    <tr>
                        <td><input class="form-control" id="tNum1" name="cNum1" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                        <td><input class="form-control" id="tDesc1" name="cDesc1" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                        <td><input class="form-control" id="tSerieE1" name="cSerieE1" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                        <td><input class="form-control" id="tCarga1" name="cCarga1" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                        
                    </tr>
                    </tbody>
                    </div>
            </div>


            <!-- Exercício Ombros -->
            <div class="form-group">
                <h3>Ombros</h3>
                <div class="col-md-4">
                    <tbody>
                    <tr>
                        <td><input class="form-control" id="tNum2" name="cNum2" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                        <td><input class="form-control" id="tDesc2" name="cDesc2" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                        <td><input class="form-control" id="tSerieE2" name="cSerieE2" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                        <td><input class="form-control" id="tCarga2" name="cCarga2" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                        
                    </tr>
                    </tbody>
                </div>
            </div>


            <!-- Exercício Glúteos -->
            <div class="form-group">
                <h3>Glúteos</h3>
                <div class="col-md-4">
                    <tbody>
                    <tr>
                        <td><input class="form-control" id="tNum3" name="cNum3" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                        <td><input class="form-control" id="tDesc3" name="cDesc3" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                        <td><input class="form-control" id="tSerieE3" name="cSerieE3" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                        <td><input class="form-control" id="tCarga3" name="cCarga3" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                       
                    </tr>
                    </tbody>
                </div>
            </div>


            <!-- Exercício Tríceps -->
            <div class="form-group">
                <h3>Tríceps</h3>
                <div class="col-md-4">
                    <tbody>
                    <tr>
                        <td><input class="form-control" id="tNum4" name="cNum4" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                        <td><input class="form-control" id="tDesc4" name="cDesc4" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                        <td><input class="form-control" id="tSerieE4" name="cSerieE4" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                        <td><input class="form-control" id="tCarga4" name="cCarga4" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                       
                    </tr>
                    </tbody>
                </div>
            </div>


            <!-- Exercício Peitoral -->
            <div class="form-group">
                <h3>Peitoral</h3>
                <div class="col-md-4">
                    <tbody>
                    <tr>
                        <td><input class="form-control" id="tNum5" name="cNum5" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                        <td><input class="form-control" id="tDesc5" name="cDesc5" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                        <td><input class="form-control" id="tSerieE5" name="cSerieE5" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                        <td><input class="form-control" id="tCarga5" name="cCarga5" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                       
                    </tr>
                    </tbody>
                </div>
            </div>


            <!-- Exercício Pernas -->
            <div class="form-group">
                <h3>Pernas</h3>
                <div class="col-md-4">
                    <tbody>
                    <tr>
                        <td><input class="form-control" id="tNum6" name="cNum6" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                        <td><input class="form-control" id="tDesc6" name="cDesc6" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                        <td><input class="form-control" id="tSerieE6" name="cSerieE6" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                        <td><input class="form-control" id="tCarga6" name="cCarga6" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                        
                    </tr>
                    </tbody>

                </div>
            </div>

        <!-- Exercício Abdominais -->
        <div class="form-group">
            <h3>Abdominais</h3>
            <div class="col-md-4">
                <tbody>
                <tr>
                    <td><input class="form-control" id="tNum7" name="cNum7" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                    <td><input class="form-control" id="tDesc7" name="cDesc7" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                    <td><input class="form-control" id="tSerieE7" name="cSerieE7" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                    <td><input class="form-control" id="tCarga7" name="cCarga7" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                   
                </tr>
                </tbody>

            </div>
        </div>

        <!-- Exercício Aerobica -->
        <div class="form-group">
            <h3>Aeróbica</h3>
            <div class="col-md-4">
                <tbody>
                <tr>
                    <td><input class="form-control" id="tNum8" name="cNum8" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                    <td><input class="form-control" id="tDesc8" name="cDesc8" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                    <td><input class="form-control" id="tSerieE8" name="cSerieE8" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                    <td><input class="form-control" id="tCarga8" name="cCarga8" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                    
                </tr>
                </tbody>

            </div>
        </div>

        <!-- Exercício Ginástica -->
        <div class="form-group">
            <h3>Ginástica</h3>
            <div class="col-md-4">
                <tbody>
                <tr>
                    <td><input class="form-control" id="tNum9" name="cNum9" type="number" placeholder="N" size="5" min="0" max="99"/></td>
                    <td><input class="form-control" id="tDesc9" name="cDesc9" type="text" placeholder="Descrição" size="30" maxlength="30"/></td>
                    <td><input class="form-control" id="tSerieE9" name="cSerieE9" type="text" placeholder="Serie" size="15" maxlength="15"/></td>
                    <td><input class="form-control" id="tCarga9" name="cCarga9" type="text" placeholder="Carga" size="15" maxlength="15" /></td>
                    
                </tr>
                </tbody>

            </div>
        </div>
<br><br>
            <!-- Botões Salvar,Finalizar,Cancelar -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="tSalvart"></label>
                <div class="col-md-8">
                    <button id="tSalvart" name="cSalvar" class="btn btn-primary">Salvar</button>
                    <a href="administradores.php"><button id="tFinalizar" name="cFinalizar" type="button" class="btn btn-success" value="finalizar">Finalizar Cadastro</button></a>
                    
                </div>
            </div>

    </fieldset>
</form>
<footer id="rodape">
    <p>Copyright 2016 - by <a href="https://www.facebook.com/marajobr">  Leandro Queiroz </a><br>
        <a href="img/facebook.png" target="parent">
            <img src="img/facebook.png" width="25" height="25"></a>

        <a href="img/twitter.png" target="parent">
            <img src="img/twitter.png" width="25" height="25"/></a>
    </p>
</footer>
        
        
        
        
        
        
        
        
    </body>
</html>
