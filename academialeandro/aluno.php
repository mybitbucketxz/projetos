<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of aluno
 *
 * @author Willian
 */
class aluno { // projeto de um aluno.
   
    
    
    private $idAluno,$login,$senha,$nome,$dataAvaliacao,$dataReavaliacao,$inicio,$idade,$objetivos,$observacoes,$situacao;
    //Declaraçao dos atributo de um aluno.
    
    
   
    function __construct($idAluno,$login, $senha, $nome, $dataAvaliacao, $dataReavaliacao, $inicio, $idade, $objetivos, $observacoes, $situacao) {
        $this->idAluno;
        $this->login = $login;
        $this->senha = md5($senha."indiscriptografavel");
        $this->nome = $nome;
        $this->dataAvaliacao = $dataAvaliacao;
        $this->dataReavaliacao = $dataReavaliacao;
        $this->inicio = $inicio;
        $this->idade = $idade;
        $this->objetivos = $objetivos;
        $this->observacoes = $observacoes;
        $this->situacao = $situacao;
    }

    
    function getIdAluno() {
        return $this->idAluno;
    }

    function getLogin() {
        return $this->login;
    }

    function getSenha() {
        return $this->senha;
    }

    function getNome() {
        return $this->nome;
    }

    function getDataAvaliacao() {
        return $this->dataAvaliacao;
    }

    function getDataReavaliacao() {
        return $this->dataReavaliacao;
    }

    function getInicio() {
        return $this->inicio;
    }

    function getIdade() {
        return $this->idade;
    }

    function getObjetivos() {
        return $this->objetivos;
    }

    function getObservacoes() {
        return $this->observacoes;
    }

    function getSituacao() {
        return $this->situacao;
    }

    function setIdAluno($idAluno) {
        $this->idAluno = $idAluno;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setDataAvaliacao($dataAvaliacao) {
        $this->dataAvaliacao = $dataAvaliacao;
    }

    function setDataReavaliacao($dataReavaliacao) {
        $this->dataReavaliacao = $dataReavaliacao;
    }

    function setInicio($inicio) {
        $this->inicio = $inicio;
    }

    function setIdade($idade) {
        $this->idade = $idade;
    }

    function setObjetivos($objetivos) {
        $this->objetivos = $objetivos;
    }

    function setObservacoes($observacoes) {
        $this->observacoes = $observacoes;
    }

    function setSituacao($situacao) {
        $this->situacao = $situacao;
    }


    
    
    
}
