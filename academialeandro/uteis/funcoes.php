<?php
// Funcão para verificar se a senha está criptografada.



function verificarSenha($senha){
    
    $quantDigitos = strlen($senha);
    
    
    
    if($quantDigitos == 32){
        return false;
    }
    
    return true;
    
}

//Função para verificar se é vaziu o dado (utilizado para quando o aluno logar)
function verificarVazio($dado){
    if(empty($dado)){
        return "Indisponivel"; // Se o dado for vaziu ele irá retornar um aviso.
    }
    
    return $dado; // Se não for vaziu ele prosseguira com o código e irá retornar o próprio dado.
}


//Funcão utilizada para imprimir o cadastro do aluno(utilizado para imprimir relatorio)
function imprimirRelatorio($arrayMulti){
      
       
      
      
       require('pdf/fpdf.php');
        
        $pdf = new FPDF('P','cm','A4'); // O 'P' É em modo retrato, se botar 'F' é modo paisagem.
      
        $pdf->AddPage();
        
          
        // DADOS PESSOAIS
        $pdf->SetFont("Arial","B","20"); // Definimos a fonte dessa célula
        $pdf->Cell(19,1,  utf8_decode($arrayMulti['aluno'][0]),1,1,"C");
         //COMPRIMENTO,LARGURA,TEXTO,BORDA,QUEBRA A LINHA,ALINHAMENTO
        
        
        $pdf->SetFont("Arial","B","12"); // Definimos a fonte dessa célula
        
        $pdf->Cell(5,1,"Inicio: ".$arrayMulti['aluno'][3],1,0,"L"); //A Célula está definida assim:
                                                     //Comprimento,Altura,Texto/imagem,com/sem bordas,posição('C',L','R') 
        
        
       
        $pdf->Cell(7,1,utf8_decode("Data Da Avaliação: ").$arrayMulti['aluno'][1],1,0,"L"); //A Célula está definida assim:
                                                     //Comprimento,Altura,Texto/imagem,com/sem bordas,posição('C',L','R') 
        
        $pdf->Cell(7,1,utf8_decode("Data Da Reavaliação: ").$arrayMulti['aluno'][2],1,1,"L");
         //COMPRIMENTO,LARGURA,TEXTO,BORDA,QUEBRA A LINHA,ALINHAMENTO
        
       $pdf->Cell(19,1,"Objetivos: ".utf8_decode($arrayMulti['aluno'][5]),1,1,"L");
        //COMPRIMENTO,LARGURA,TEXTO,BORDA,QUEBRA A LINHA,ALINHAMENTO
      
       $pdf->Cell(19,1,utf8_decode("Observação: ").utf8_decode($arrayMulti['aluno'][6]),1,1,"L");
         //COMPRIMENTO,LARGURA,TEXTO,BORDA,QUEBRA A LINHA,ALINHAMENTO
       
       
       
       //EXERCICIOS
       
       $arrayExercicio[] = "Num: ";
       $arrayExercicio[] = utf8_decode("Descrição: ");
       $arrayExercicio[] = "Tipo do exercicio: ";
       $arrayExercicio[] = utf8_decode("Serie: ");
       $arrayExercicio[] = "Carga: ";
      
       
      //Esse codigo abaixo, irá percorrer os exercicios, verificando antes de imprimir uma célula, qual
     // deve ser impressa.
       $d = 0;
       for($i = 0;$i < count($arrayMulti['exercicios']);$i++){
          
           if($d < count($arrayExercicio)){
                switch ($d) {
                    case 0:
                        $pdf->Cell(19,1,$arrayExercicio[0].$arrayMulti['exercicios'][$i],1,1,"L");
                         $d++;
                      break;
                     case 1:
                        $pdf->Cell(19,1,$arrayExercicio[1].utf8_decode($arrayMulti['exercicios'][$i]),1,1,"L");
                          $d++;
                      break;
                   case 2:
                        $pdf->Cell(19,1,$arrayExercicio[2].utf8_decode($arrayMulti['exercicios'][$i]),1,1,"L");
                        $d++;
                      break;
                   case 3:
                        $pdf->Cell(9,1,$arrayExercicio[3].$arrayMulti['exercicios'][$i],1,0,"L");
                        $d++;
                      break;
                   case 4:
                        $pdf->Cell(10,1,$arrayExercicio[4].$arrayMulti['exercicios'][$i],1,1,"L");
                        $pdf->Cell(19,1," ",1,1,"L");
                        $d = 0;
                      break;
                   

                }
                
           }
          
          
           
       }
       
    
        
        ob_start (); // isso fez com que funcionase.
        $pdf->Output();
    
    
    
}



?>
