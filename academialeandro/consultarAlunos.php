<?php


//AÇÃO: 
//1 - CONSULTAR CADASTRO DO ALUNO
//2 - CONSULTAR EXERCICIO DO ALUNO
//3 - EXCLUIR CADASTRO INTEIRO DO ALUNO
//4 - UPDATE CADASTRO DO ALUNO 
//5 - UPDATE EXERCICIO DO ALUNO


header("Content-Type: text/html; charset=windows-1252",true); // PARA NÃO APARECEREM CARACTERES ESTRANHOS!

include_once 'aluno.php';
include_once 'exercicio.php';
include_once 'conexao/dataBase.php';
include_once 'uteis/funcoes.php';

session_start();

if(!isset($_SESSION['admin']) && ($_SESSION['admin'] != true)){
      header("location: index.html");
      die();
}


?>


<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="css/estilo.css" rel="stylesheet">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <style>
            body{
                margin: 30px;
            }
            
            div{
                border-style: solid;
            }
            
            div p{
                margin: 10px;
            }
            
            div h3{
                margin: 5px;
            }
        </style>
    </head>
    <body>
        
     <div class="container">
   <h2>Consultar Aluno</h2>
 
   <form role="form" action="" method="POST">
    <div class="form-group">       
      <label for="sel1">Pesquisar por:</label>
      <select class="form-control" id="sel1" name="opcao">
        <option value="nome">Nome</option>
        <option value="dataAvaliacao">Data de Avaliação</option>
        <option value="dataReavaliacao">Data de Reavaliação</option>
        <option value="inicio">Inicio</option>
        <option value="idade">Idade</option>
        <option value="objetivos">Objetivos</option>
        <option value="observacoes">Observações</option>
        <option value="situacao">Situação</option>
      </select>
      <input type="text" class="form-control" id="usr" name="consultar"> </br>
      <button type='submit' class='btn btn-primary btn-lg' style="margin-right: 5px;">Pesquisar</button> 
       <a href="administradores.php"> <button type='button' class='btn btn-primary btn-lg'>Voltar</button> </a>
    </div>
  </form>
   
  
     
</br>
   
        
        <?php
        
        
       // if(){
            
       // }
            

           $dataBase = new database();
           
           

           if(isset($_POST['opcao'])&& empty($_POST['consultar'])){
               
            $sqlalunos = "SELECT * FROM aluno;";
            $selecionarAlunos =  $dataBase->selectDB($sqlalunos);

            

            echo '<h2>Lista de alunos:</h2>';
            while($linhasAlunos = mysqli_fetch_assoc($selecionarAlunos)){
              echo '<div>';
              echo '<h3>Informações:</h3>';
              echo '<p>ID ALUNO: '.$linhasAlunos['idAluno'].'</p>';
              echo '<p>LOGIN: '.$linhasAlunos['login'].'</p>';
              echo '<p>SENHA: '.$linhasAlunos['senha'].'</p>';
              echo '<p>NOME: '.$linhasAlunos['nome'].'</p>';
              echo '<p>DATA DE AVALIAÇÃO: '.$linhasAlunos['dataAvaliacao'].'</p>';
              echo '<p>DATA DE REAVALIAÇÃO: '.$linhasAlunos['dataReavaliacao'].'</p>';
              echo '<p>INICIO: '.$linhasAlunos['inicio'].'</p>';
              echo '<p>IDADE: '.$linhasAlunos['idade'].'</p>';
              echo '<p>OBJETIVOS: '.$linhasAlunos['objetivos'].'</p>';
              echo '<p>OBSERVAÇÕES: '.$linhasAlunos['observacoes'].'</p>';
              echo '<p>SITUAÇÃO: '.$linhasAlunos['situacao'].'</p>';
              echo '<hr/>';


              $sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=".$linhasAlunos['idAluno'].";";
              
              $selecionarExercicios =  $dataBase->selectDB($sqlExercicios);

              echo '<h3>Exercicios Praticados:</h3>'; 
              while($linhasExercicios = mysqli_fetch_assoc($selecionarExercicios)){

                    echo '<p>NUMERO: '.$linhasExercicios['num'].'</p>';
                    echo '<p>DESCRIÇÃO: '.$linhasExercicios['descricao'].'</p>';
                    echo '<p>TIPO DO EXERCICIO: '.$linhasExercicios['tipoExercicio'].'</p>';
                    echo '<p>SÉRIE: '.$linhasExercicios['serie'].'</p>';
                    echo '<p>CARGA: '.$linhasExercicios['carga'].'</p>';
                    echo '</br>';
              }
            echo  "<a href='consultarAlunos.php?cod=".$linhasAlunos['idAluno']."&acao=1'><button type='button' class='btn btn-primary btn-lg' style='margin-right: 20px;'>Editar Cadastro</button></a>";
            echo  "<a href='consultarAlunos.php?cod=".$linhasAlunos['idAluno']."&acao=2'><button type='button' class='btn btn-primary btn-lg' style='margin-right: 20px;'>Editar Exercicio</button></a>";
            echo  "<a href='consultarAlunos.php?cod=".$linhasAlunos['idAluno']."&acao=3'><button type='button' class='btn btn-primary btn-lg'>Deletar Cadastro</button></a>";
            echo '</div>';
            echo '<hr/>';
              

            } // Fim da consulta dos alunos   
           
           }elseif (isset($_POST['opcao']) && isset($_POST['consultar'])) {
    
            $opcao = $_POST['opcao'];
            $consultar = $_POST['consultar'];
               
            $sqlalunos = "SELECT * FROM aluno WHERE  ".$opcao." like '%".$consultar."%';";
            $selecionarAlunos =  $dataBase->selectDB($sqlalunos);



            echo '<h2>Lista de alunos:</h2>';
            while($linhasAlunos = mysqli_fetch_assoc($selecionarAlunos)){
              echo '<div>';
              echo '<h3>Informações:</h3>';
              echo '<p>ID ALUNO: '.$linhasAlunos['idAluno'].'</p>';
              echo '<p>LOGIN: '.$linhasAlunos['login'].'</p>';
              echo '<p>SENHA: '.$linhasAlunos['senha'].'</p>';
              echo '<p>NOME: '.$linhasAlunos['nome'].'</p>';
              echo '<p>DATA DE AVALIAÇÃO: '.$linhasAlunos['dataAvaliacao'].'</p>';
              echo '<p>DATA DE REAVALIAÇÃO: '.$linhasAlunos['dataReavaliacao'].'</p>';
              echo '<p>INICIO: '.$linhasAlunos['inicio'].'</p>';
              echo '<p>IDADE: '.$linhasAlunos['idade'].'</p>';
              echo '<p>OBJETIVOS: '.$linhasAlunos['objetivos'].'</p>';
              echo '<p>OBSERVAÇÕES: '.$linhasAlunos['observacoes'].'</p>';
              echo '<p>SITUAÇÃO: '.$linhasAlunos['situacao'].'</p>';
              echo '<hr/>';


              $sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=".$linhasAlunos['idAluno'].";";
              //$sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=2;";
              $selecionarExercicios =  $dataBase->selectDB($sqlExercicios);

              echo '<h3>Exercicios Praticados:</h3>'; 
              while($linhasExercicios = mysqli_fetch_assoc($selecionarExercicios)){

                    echo '<p>NUMERO: '.$linhasExercicios['num'].'</p>';
                    echo '<p>DESCRIÇÃO: '.$linhasExercicios['descricao'].'</p>';
                    echo '<p>TIPO DO EXERCICIO: '.$linhasExercicios['tipoExercicio'].'</p>';
                    echo '<p>SÉRIE: '.$linhasExercicios['serie'].'</p>';
                    echo '<p>CARGA: '.$linhasExercicios['carga'].'</p>';
                    
              }
            echo  "<a href='consultarAlunos.php?cod=".$linhasAlunos['idAluno']."&acao=1'><button type='button' class='btn btn-primary btn-lg' style='margin-right: 20px;'>Editar Cadastro</button></a>";
            echo  "<a href='consultarAlunos.php?cod=".$linhasAlunos['idAluno']."&acao=2'><button type='button' class='btn btn-primary btn-lg' style='margin-right: 20px;'>Editar Exercicio</button></a>";
            echo  "<a href='consultarAlunos.php?cod=".$linhasAlunos['idAluno']."&acao=3'><button type='button' class='btn btn-primary btn-lg'>Deletar Cadastro</button></a>";
            echo '</div>';
            echo '<hr/>';
              

            } // Fim da consulta dos alunos   
               
          }elseif (isset($_GET['cod']) && $_GET['acao'] == 1) {
              
            $idAluno = $_GET['cod'];
            $acao = $_GET['acao'];
            
            $sqlalunos = "SELECT * FROM aluno WHERE idAluno = ".$idAluno.";";
            $selecionarAlunos =  $dataBase->selectDB($sqlalunos);
              
            
            while($linhasAlunos = mysqli_fetch_assoc($selecionarAlunos)){
              echo '<div>';
              echo "<form role='form' action='consultarAlunos.php?cod=".$linhasAlunos['idAluno']."&acao=4' method='POST'>";
              echo '<h3>Informações:</h3>';             
              echo '<p>LOGIN:</p>';
              echo "<input type='text' class='form-control' name='cLogin' value='".$linhasAlunos['login']."'> </br>";
              echo '<p>SENHA:</p>';
              echo "<input type='text' class='form-control' name='cSenha' value='".$linhasAlunos['senha']."'> </br>";
              echo '<p>NOME:</p>';
              echo "<input type='text' class='form-control' name='cNome' value='".$linhasAlunos['nome']."'> </br>";
              echo '<p>DATA DE AVALIAÇÃO:</p>';
              echo "<input type='text' class='form-control' name='cDataAvaliacao' value='".$linhasAlunos['dataAvaliacao']."'> </br>";
              echo '<p>DATA DE REAVALIAÇÃO:</p>';
              echo "<input type='text' class='form-control' name='cDataReavaliacao' value='".$linhasAlunos['dataReavaliacao']."'> </br>";
              echo '<p>INICIO:</p>';
              echo "<input type='text' class='form-control' name='cInicio' value='".$linhasAlunos['inicio']."'> </br>";
              echo '<p>IDADE: </p>';
              echo "<input type='text' class='form-control' name='cIdade' value='".$linhasAlunos['idade']."'> </br>";
              echo '<p>OBJETIVOS:</p>';
              echo "<input type='text' class='form-control' name='cObjetivos' value='".$linhasAlunos['objetivos']."'> </br>";
              echo '<p>OBSERVAÇÕES: </p>';
              echo "<input type='text' class='form-control' name='cObservacoes' value='".$linhasAlunos['observacoes']."'> </br>";
              echo '<p>SITUAÇÃO: </p>';
              echo "<input type='text' class='form-control' name='cSituacao' value='".$linhasAlunos['situacao']."'> </br>";
              echo '<hr/>';

           
            echo  "<button type='submit' class='btn btn-primary btn-lg'>Editar Cadastro</button>";
            
            echo '</div>';
            echo '<hr/>';
            echo "</form>";
              

            } // Fim da consulta dos alunos  
           
            
              
          }elseif (isset($_GET['cod']) && $_GET['acao'] == 2) {
              
            $idAluno = $_GET['cod'];
            $acao = $_GET['acao'];
            
            $cExercicios = 0;
            
              $sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=".$idAluno.";";
              //$sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=2;";
              $selecionarExercicios =  $dataBase->selectDB($sqlExercicios);

              echo '<h3>Exercicios Praticados:</h3>'; 
              
              while($linhasExercicios = mysqli_fetch_assoc($selecionarExercicios)){
                    echo '<div>';
                    echo "<form role='form' action='consultarAlunos.php?cod=".$idAluno."&acao=5' method='POST'>";
                    echo "<input type='hidden' name='cIdExercicio".$cExercicios."' value='".$linhasExercicios['idExercicio']."'>";
                    echo '<p>NUMERO:</p>';
                    echo "<input type='text' class='form-control' name='cNumero".$cExercicios."' value='".$linhasExercicios['num']."'> </br>";
                    echo '<p>DESCRIÇÃO: </p>';
                    echo "<input type='text' class='form-control' name='cDescricao".$cExercicios."' value='".$linhasExercicios['descricao']."'> </br>";
                    echo '<p>TIPO DO EXERCICIO:</p>';
                    echo "<input type='text' class='form-control' name='cTipoExercicio".$cExercicios."' value='".$linhasExercicios['tipoExercicio']."'> </br>";
                    echo '<p>SÉRIE:</p>';
                    echo "<input type='text' class='form-control' name='cSerie".$cExercicios."' value='".$linhasExercicios['serie']."'> </br>";
                    echo '<p>CARGA:</p>';
                    echo "<input type='text' class='form-control' name='cCarga".$cExercicios."' value='".$linhasExercicios['carga']."'> </br>";
                    $cExercicios++;
                     echo '</div>';
                    
              }
             
            echo "<input type='hidden' name='quantE' value='".$cExercicios."'>";
            echo  "<button type='submit' class='btn btn-primary btn-lg'>Editar Exercicio</button>";
            
           
            echo '<hr/>';
            echo "</form>";
              

           
           // fim do consultar exercicio
            
              
          }elseif (isset($_GET['cod']) && $_GET['acao'] == 3) {
    
            $idAluno = $_GET['cod'];
            $acao = $_GET['acao'];
            
            $sqlalunos = "DELETE FROM aluno WHERE idAluno = ".$idAluno.";";
            $selecionarAlunos =  $dataBase->deleteDB($sqlalunos);
            
            echo "<h2 style='color: green;'> Aluno deletado com sucesso!</h2>";
            
            
           
          }elseif (isset($_GET['cod']) && $_GET['acao'] == 4) { // UPDATE CADASTRO DO ALUNO
    
          
            $idAluno = $_GET['cod'];
            

                $arrayPosts[] = $_POST['cLogin'];
                $arrayPosts[] = $_POST['cSenha'];
                $arrayPosts[] = $_POST['cNome'];
                $arrayPosts[] = $_POST['cDataAvaliacao'];
                $arrayPosts[] = $_POST['cDataReavaliacao'];
                $arrayPosts[] = $_POST['cInicio'];
                $arrayPosts[] = $_POST['cIdade'];
                $arrayPosts[] = $_POST['cObjetivos'];
                $arrayPosts[] = $_POST['cObservacoes'];
                $arrayPosts[] = $_POST['cSituacao'];


                for($i = 0; $i < count($arrayPosts);$i++){
                if(!isset($arrayPosts[$i]) && empty($arrayPosts[$i])){
                     $arrayPosts[$i] = '';
                 }
                }

                // Instanciamos um novo aluno.
                $aluno = new aluno($idAluno,
                        $arrayPosts[0],
                        $arrayPosts[1],
                        $arrayPosts[2],
                        $arrayPosts[3],
                        $arrayPosts[4],
                        $arrayPosts[5],
                        $arrayPosts[6],
                        $arrayPosts[7],
                        $arrayPosts[8],
                        $arrayPosts[9]);


                // Note que o aluno tem o atributo idAluno mas não é necessario colocar no insert, pois o id é auto increment.
                //o id do aluno servira para um select ou update.
                
                if(verificarSenha($arrayPosts[1])){
                    // Aqui verificamos se a senha é a senha criptografada que por padrão tem 32 digitos
                    // se a senha não for a senha criptografada então será preciso alterar a senha.
                 $sqlUpdateAlunos = "UPDATE aluno set "
                        . " login = '".$aluno->getLogin()."',"
                        . " senha = '".$aluno->getSenha()."',"
                        . " nome = '".$aluno->getNome()."',"
                        . " dataAvaliacao = '".$aluno->getDataAvaliacao()."',"
                        . " dataReavaliacao = '".$aluno->getDataReavaliacao()."',"
                        . " inicio = '".$aluno->getInicio()."',"
                        . " idade = '".$aluno->getIdade()."',"
                        . " objetivos = '".$aluno->getObjetivos()."',"
                        . " observacoes = '".$aluno->getObservacoes()."',"
                        . " situacao = '".$aluno->getSituacao()."' "
                        . " WHERE idAluno = ".$idAluno.";";
                 
               
                
                }else{ // se for a senha criptografada então não altere a senha.
                    $sqlUpdateAlunos = "UPDATE aluno set "
                        . " login = '".$aluno->getLogin()."',"
                        . " nome = '".$aluno->getNome()."',"
                        . " dataAvaliacao = '".$aluno->getDataAvaliacao()."',"
                        . " dataReavaliacao = '".$aluno->getDataReavaliacao()."',"
                        . " inicio = '".$aluno->getInicio()."',"
                        . " idade = '".$aluno->getIdade()."',"
                        . " objetivos = '".$aluno->getObjetivos()."',"
                        . " observacoes = '".$aluno->getObservacoes()."',"
                        . " situacao = '".$aluno->getSituacao()."' "
                        . " WHERE idAluno = ".$idAluno.";";
                }
                
                
                
                $updateAlunos =  $dataBase->updateDB($sqlUpdateAlunos); // Aqui cadastra os novos dados do aluno.
                
               
            echo "<h2 style='color: green;'> O cadastro do aluno(a) foi  editado com sucesso!</h2>";
           // header("locaton: consultarAlunos.php?msg=3"); // Avise que foi atualizado ao salvar.
            
          }elseif (isset($_GET['cod']) && $_GET['acao'] == 5) {
    
          
            $idAluno = $_GET['cod'];
            

                $cIdExercicio = '';
                $cNumero = '';
                $cDescricao = '';
                $cTipoExercicio = '';
                $cSerie = '';
                $cCarga = '';
                
                if(isset( $_POST['quantE']) &&  $_POST['quantE'] != 0){
                    $quantidadeExercicio = $_POST['quantE'];
                    for($c = 0; $c < $quantidadeExercicio;$c++){
                        
                          if(isset($_POST["cIdExercicio".$c])){
                             $cIdExercicio = $_POST["cIdExercicio".$c];
                         }
                        
                         if(isset($_POST["cNumero".$c])){
                             $cNumero = $_POST["cNumero".$c];
                         }
                         
                         if(isset($_POST["cDescricao".$c])){
                             $cDescricao = $_POST["cDescricao".$c];
                         }
                         
                         if(isset($_POST["cTipoExercicio".$c])){
                             $cTipoExercicio = $_POST["cTipoExercicio".$c];
                         }
                         
                         if(isset($_POST["cSerie".$c])){
                             $cSerie = $_POST["cSerie".$c];
                         }
                         
                         if(isset($_POST["cCarga".$c])){
                             $cCarga = $_POST["cCarga".$c];
                         }
                             
                         $exercicio = new exercicio($cNumero,
                                 $cDescricao,
                                 $cTipoExercicio,
                                 $cSerie,
                                 $cCarga,
                                 $idAluno);
                         
                         $exercicio->setIdExercicio($cIdExercicio);
                         
                         
                         $arrayexercicios[] = $exercicio; // armazenamos as instancias dentro de um array.
                        
                     } // termina o for de percorrer o que veio do post.
                     
                     
                      
                     //enquanto 0 < 10 faça:
                      for($i = 0;$i < count($arrayexercicios);$i++){ // to achando que ele não vai entrar no loop se o array tiver 0 posição.
                       
                        $sqlUpdateExercicios = "UPDATE exercicio SET num = '".$arrayexercicios[$i]->getNum()."',"
                                
                                . "descricao = '".$arrayexercicios[$i]->getDescricao()."',"
                                . "tipoExercicio = '".$arrayexercicios[$i]->getTipoExercicio()."',"
                                . "serie = '".$arrayexercicios[$i]->getSerie()."',"
                                . "carga = '".$arrayexercicios[$i]->getCarga()."' "
                                
                                . " WHERE alunoIdAluno = ".$idAluno." AND idExercicio = ".$arrayexercicios[$i]->getIdExercicio().";";

                          $dataBase->updateDB($sqlUpdateExercicios);
                      }
                    
                }
                
                
              
            
           
            echo "<h2 style='color: green;'> Os exercicios do aluno(a)  foram  editados com sucesso!</h2>";
           // header("locaton: consultarAlunos.php?msg=3"); // Avise que foi atualizado ao salvar.
            
          }
           
           
           ?>
    </body>
</html>
