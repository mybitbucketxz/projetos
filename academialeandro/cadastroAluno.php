<?php 

session_start(); 

 if(!isset($_SESSION['admin']) && ($_SESSION['admin'] != true)){
     
     header("location: Index.html");
     
 }

?>



<!DOCTYPE html>
<html lang="en">
    <meta charset="UTF-8">
    <title>Cadastro</title>
     <link href="css/estilo.css" rel="stylesheet">
     <link href="css/bootstrap.css" rel="stylesheet">
    <script src="js/bootstrap.js">    </script>
</head>
<body>
    <form class="form-horizontal" action="cadastrarAluno.php" method="POST">
    <fieldset>

        <!-- Form Name -->
        <legend>CADASTRO DO ALUNO</legend>

        
         <!-- LOGIN -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tLogin">Login</label>
            <div class="col-md-5">
                <input id="tLogin" name="tLogin" type="text" placeholder="Login do aluno" class="form-control input-md">

            </div>
        </div>
         
         
          <!-- SENHA -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tSenha">Senha</label>
            <div class="col-md-5">
                <input id="tSenha" name="tSenha" type="password" placeholder="Senha do aluno" class="form-control input-md">

            </div>
        </div>
          
          
        
        <!-- NOME-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tNome">Nome</label>
            <div class="col-md-5">
                <input id="tNome" name="tNome" type="text" placeholder="Nome Completo" class="form-control input-md">

            </div>
        </div>
        
       

        <!-- IDADE-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tIdade">Idade</label>
            <div class="col-md-1">
                <input id="tIdade" name="tIdade" type="number" placeholder="99" min="0" max="99" class="form-control input-md">

            </div>
        </div>

        <!-- Data Avaliação-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tAva">Data da Avaliação</label>
            <div class="col-md-2">
                <input id="tAva" name="tAva" type="date" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Data de Inicio-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tInicio">Inicio</label>
            <div class="col-md-2">
                <input id="tInicio" name="tInicio" type="date" placeholder="DD/MM/AA" class="form-control input-md">

            </div>
        </div>

        <!-- Data de Reavaliação-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tReav">Data da Reavaliação</label>
            <div class="col-md-2">
                <input id="tReav" name="tReav" type="date" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Objetivos-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tObje">Objetivos</label>
            <div class="col-md-5">
                <input id="tObje" name="tObje" type="text" placeholder="" class="form-control input-md">

            </div>
        </div>

        <!-- Observação -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="tObs">Observações</label>
            <div class="col-md-4">
                <textarea class="form-control" id="tObs" name="tObs"></textarea>
            </div>
        </div>

        

               
        <br>
        <!-- Botão Salvar e Cancelar -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="button1id"></label>
            <div class="col-md-8">
                <button  id="tSave" name="cSave" class="btn btn-success">Salvar</button>
                <button  id="tCancelar" name="cCancelar" class="btn btn-inverse" value="cancelar">Cancelar</button>
            </div>
        </div>
</fieldset>
</form>

   

</body>
</html>