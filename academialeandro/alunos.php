<?php 

include_once 'aluno.php'; // Os includes sempre devem vir antes da sessão, se não da erro!!
include_once 'exercicio.php';
include_once 'uteis/funcoes.php';
// Se não tiver incluido a classe do objeto que está na sessão também irá resultar em erro!!

session_start();

header("Content-Type: text/html; charset=UTF-8",true); 

//AÇÕES: 
//1 - INFORMAÇÕES PESSOAIS
//2 - EXERCICIOS
//3 - SAIR
//4 - IMPRIMIR FICHA


 if(isset($_GET['acao']) && $_GET['acao'] == 3){
    session_destroy();
    header("location: index.html");
  }
  
 

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    <head>
        <link href="css/estilo.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <style>
            
            body{
               padding-left: 20px;
            }
            
           
            
            div{
                margin: 30px;
            }
            
            div p{
                margin: 20px;
            }
            
            div h1{
                margin: 10px;
            }
            
             div h2{
                margin: 15px;
            }
            
            a{
                margin: 10px;
            }
            </style>
  </head>
    <body>
        <?php
        
         if(isset($_SESSION['alunovalido']) && $_SESSION['alunovalido'] = true){
          
             
            $nome = verificarVazio($_SESSION['aluno']->getNome());
            $dataAvaliacao = verificarVazio($_SESSION['aluno']->getDataAvaliacao());
            $dataReavaliacao = verificarVazio($_SESSION['aluno']->getDataReavaliacao());
            $inicio = verificarVazio($_SESSION['aluno']->getInicio());
            $idade = verificarVazio($_SESSION['aluno']->getIdade());
            $objetivos = verificarVazio($_SESSION['aluno']->getObjetivos());
            $observacoes = verificarVazio($_SESSION['aluno']->getObservacoes());
            $situacao = verificarVazio($_SESSION['aluno']->getSituacao());
            
          echo '<h1>Bem vindo aluno: '.$nome.'</h1>'; 
       
          
         if(isset($_GET['acao']) && $_GET['acao'] == 1){
            
              /* EXIBIÇÃO ANTIGA.
            echo '<hr/>';  
            echo '<h2> Informações Pessoais: </h2>';
            
            echo "<p> <b>Nome:</b> " .$_SESSION['aluno']->getNome()."</p>";
            echo "<p> <b>Data de Avaliacao:</b> " .$_SESSION['aluno']->getDataAvaliacao()."</p>";
            echo "<p> <b>Data da Reavaliacao:</b> " .$_SESSION['aluno']->getDataReavaliacao()."</p>";
            echo "<p> <b>Inicio:</b> " .$_SESSION['aluno']->getInicio()."</p>";
            echo "<p> <b>Idade:</b> " .$_SESSION['aluno']->getIdade()."</p>";
            echo "<p> <b>Objetivos:</b> " .$_SESSION['aluno']->getObjetivos()."</p>";
            echo "<p> <b>Observacoes:</b> " .$_SESSION['aluno']->getObservacoes()."</p>";
            echo "<p> <b>Situacao:</b> " .$_SESSION['aluno']->getSituacao()."</p>";
            echo '<hr/>';
              */
            
           
            
            echo "<table class='table table-bordered'>
            
                        <th>Nome</th>
                        <th>Data de Avaliação</th>
                        <th>Data de Reavaliação</th>
                        <th>Inicio</th>
                        <th>Idade</th>
                        <th>Objetivos</th>
                        <th>Observacoes</th>
                        <th>Situacao</th>

                        <tr>
                           <td class='success'>".$nome."</td>
                           <td class='success'>".$dataAvaliacao."</td>
                           <td class='success'>".$dataReavaliacao."</td>
                           <td class='success'>".$inicio."</td>
                           <td class='success'>".$idade."</td>
                           <td class='success'>".$objetivos."</td>
                           <td class='success'>".$observacoes."</td>
                           <td class='success'>".$situacao."</td>
                        </tr>
                
                 </table>";
            
            
         

          }
          
           if(isset($_GET['acao']) && $_GET['acao'] == 2){
             
               
             
               
             if(isset($_SESSION['exercicios'])){ // Se o aluno possuir exercicio faça:
               
                
                 
                 
                 
                echo "<table class='table table-bordered'>";
                echo "<th>Num</th>";
                echo "<th>Descrição</th>";
                echo "<th>Tipo de Exercicio</th>"; // Colocamos as constantes fóra do laço
                echo "<th>Serie</th>";
                echo "<th>Carga</th>";
                for($i =0 ; $i < count($_SESSION['exercicios']); $i++){
                    $num = verificarVazio($_SESSION['exercicios'][$i]->getNum());
                    $descricao = verificarVazio($_SESSION['exercicios'][$i]->getDescricao());
                    $tipoExercicio = verificarVazio($_SESSION['exercicios'][$i]->getTipoExercicio());
                    $serie = verificarVazio($_SESSION['exercicios'][$i]->getSerie());
                    $carga = verificarVazio($_SESSION['exercicios'][$i]->getCarga());
                    
                        //Vai populando as células com dados diferentes.
                         echo   "
                                
                               <tr>
                                   <td class='success'>".$num."</td>
                                   <td class='success'>".$descricao."</td>
                                   <td class='success'>".$tipoExercicio."</td>
                                   <td class='success'>".$serie."</td>
                                   <td class='success'>".$carga."</td>                                  
                                </tr>
                
                                ";

                        }
                  echo "</table>"; //Fechamos a nossa tabela após o laço for.
                    
            }else{
               echo '<p>Nenhum exercicio cadastrado no momento!</p>'; // Se não possuir, avise-o.
            }
            
            
        } //FIM DA AÇÃO 2 ( EXERCICIOS)
        
        
        
        
          if(isset($_GET['acao']) && $_GET['acao'] == 4){
              
              
            $nome = verificarVazio($_SESSION['aluno']->getNome());
            $dataAvaliacao = verificarVazio($_SESSION['aluno']->getDataAvaliacao());
            $dataReavaliacao = verificarVazio($_SESSION['aluno']->getDataReavaliacao());
            $inicio = verificarVazio($_SESSION['aluno']->getInicio());
            $idade = verificarVazio($_SESSION['aluno']->getIdade());
            $objetivos = verificarVazio($_SESSION['aluno']->getObjetivos());
            $observacoes = verificarVazio($_SESSION['aluno']->getObservacoes());
            $situacao = verificarVazio($_SESSION['aluno']->getSituacao());
              
            $arrayMulti['aluno'][] = $nome;
            $arrayMulti['aluno'][] = $dataAvaliacao;
            $arrayMulti['aluno'][] = $dataReavaliacao;
            $arrayMulti['aluno'][] = $inicio;
            $arrayMulti['aluno'][] = $idade;
            $arrayMulti['aluno'][] = $objetivos;
            $arrayMulti['aluno'][] = $observacoes;
            $arrayMulti['aluno'][] = $situacao;
            
              if(isset($_SESSION['exercicios'])){ // Se o aluno possuir exercicio faça:
               for($i =0 ; $i < count($_SESSION['exercicios']); $i++){
                    $num = verificarVazio($_SESSION['exercicios'][$i]->getNum());
                    $descricao = verificarVazio($_SESSION['exercicios'][$i]->getDescricao());
                    $tipoExercicio = verificarVazio($_SESSION['exercicios'][$i]->getTipoExercicio());
                    $serie = verificarVazio($_SESSION['exercicios'][$i]->getSerie());
                    $carga = verificarVazio($_SESSION['exercicios'][$i]->getCarga());
                    
                    
                    $arrayMulti['exercicios'][] = $num;
                    $arrayMulti['exercicios'][] = $descricao;
                    $arrayMulti['exercicios'][] = $tipoExercicio;
                    $arrayMulti['exercicios'][] = $serie;
                    $arrayMulti['exercicios'][] = $carga;
                    
                    
               }
              }
              
             imprimirRelatorio($arrayMulti);//Imprimimos o relatorio.
              //echo count($arrayMulti['exercicios']);
            
            
        } //FIM DA AÇÃO 4 ( IMPRIMIR RELATORIO)
          
        
          //Por ultimo fazemos os botões aparecerem.
          echo "<a href='alunos.php?acao=4'><button type='button' class='btn btn-primary btn-lg' style='margin-bottom: 20px;'>Imprimir ficha</button></a>";
          echo "<a href='alunos.php?acao=1'><button type='button' class='btn btn-primary btn-lg' style='margin-bottom: 20px;'>Informações Pessoais</button></a>";   
          echo "<a href='alunos.php?acao=2'><button type='button' class='btn btn-primary btn-lg' style='margin-bottom: 20px;'>Exercicios</button></a>"; 
          echo "<a href='alunos.php?acao=3'><button type='button' class='btn btn-primary btn-lg' style='margin-bottom: 20px;'>Sair</button></a>";   
          
          
         }else{
             header("location: index.html");
         }
        
        
        ?>
    </body>
</html>
