<?php


include_once 'aluno.php';  // Os includes sempre devem vir antes da sessão, se não da erro!!
include_once 'exercicio.php';
include_once 'administrador.php';
include_once 'conexao/dataBase.php';

session_start();


header("Content-Type: text/html; charset=UTF-8",true); 

if(isset($_POST['tLog']) && !empty($_POST['tLog'])){
    $login = $_POST['tLog'];   
   
}else{
    header("location: Index.html");
}


if(isset($_POST['tPass']) && !empty($_POST['tPass'])){
    $senha = $_POST['tPass'];   
    $senhacriptografada = md5($senha."indiscriptografavel");
   
}else{
    header("location: Index.html");
}


$administrador1 = new administrador("willian", "12345"); // Essas seram o login/senha de 1 administrador.
$administrador2 = new administrador("juliana", "12345"); 
$administrador3 = new administrador("robson", "12345"); 
$administrador4 = new administrador("sandro", "12345"); 
$administrador5 = new administrador("paulo", "12345"); 

//Coloquemos todo os nossos administradores dentro de um array.
$arrayAdministradores[] = $administrador1;
$arrayAdministradores[] = $administrador2;
$arrayAdministradores[] = $administrador3;
$arrayAdministradores[] = $administrador4;
$arrayAdministradores[] = $administrador5;

$loginCorreto = false;
for($i = 0;$i < 5;$i++){ // Percorremos os nossos administradores que estã odentro do array, vericando login/senha.
    if($login == $arrayAdministradores[$i]->getLogin() && $senhacriptografada == $arrayAdministradores[$i]->getSenha()){
        $loginCorreto = true; // Então se estiver certo colocamos que o login é verdadeiro.
        $nomeAdmin = $arrayAdministradores[$i]->getLogin();
    }
}

if($loginCorreto == true){
    echo 'O login e Senha está correto, pode prosseguir!';
    $_SESSION['admin'] = true;
    $_SESSION['nome'] = $nomeAdmin;
    header("location: administradores.php");
    die();
}


$dataBase = new database();

 
$sqlalunos = "SELECT * FROM aluno;";
$selecionarAlunos =  $dataBase->selectDB($sqlalunos);


while($linhasAlunos = mysqli_fetch_assoc($selecionarAlunos)){
  
    if($login == $linhasAlunos['login'] && $senhacriptografada == $linhasAlunos['senha']){
            
            $aluno = new aluno(
                    $linhasAlunos['idAluno'],
                    $linhasAlunos['login'], 
                    $linhasAlunos['senha'], 
                    $linhasAlunos['nome'], 
                    $linhasAlunos['dataAvaliacao'], 
                    $linhasAlunos['dataReavaliacao'], 
                    $linhasAlunos['inicio'], 
                    $linhasAlunos['idade'], 
                    $linhasAlunos['objetivos'], 
                    $linhasAlunos['observacoes'], 
                    $linhasAlunos['situacao']);
            
         
            $sqlExercicios = "SELECT * FROM exercicio WHERE alunoIdAluno=".$linhasAlunos['idAluno'].";";
            $selecionarExercicios =  $dataBase->selectDB($sqlExercicios);

            while($linhasExercicios = mysqli_fetch_assoc($selecionarExercicios)){
                
               //Instanciamos o nosso objeto exercicio 
                $exercicios = new exercicio(
                          $linhasExercicios['num'],
                          $linhasExercicios['descricao'],
                          $linhasExercicios['tipoExercicio'],
                          $linhasExercicios['serie'],
                          $linhasExercicios['carga'],
                          $linhasAlunos['idAluno']);
                
                //Teremos agora que colocar essas instancia dentro de um array.
                $listaDeExercicios[] = $exercicios;
                
            }
            
           
            
           
            $_SESSION['alunovalido'] = true;    
            $_SESSION['aluno'] = $aluno;
            $_SESSION['exercicios'] = $listaDeExercicios; // Passamos o nosso array para a sessão.
            header("location: alunos.php");
            die();
    }  
}



 header("location: Index.html");


?>
