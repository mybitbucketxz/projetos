

<!--# include file="manutencao.html" -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="UTF-8">   
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>SCI - Sistema de Consumo Inteligente</title>

<base href="http://www.scipiracicaba.com.br/">

<meta name="Description" content="Faça parte desta carteira de divulgadores. Mais praticidade para sua vida e melhora de renda." />

<!-- Metas Facebook -->
<meta property="og:locale" content="pt_BR">
<meta property="og:url" content="" />
<meta property="og:type" content="website">
<meta property="og:title" content="SCI - Sistema de Consumo Inteligente" />
<meta property="og:description" content="Faca parte desta carteira de divulgadores. Mais praticidade para sua vida e melhora de renda." /> 
<meta property="og:site_name" content="SCI Piracicaba">
<meta property="og:image" content="http://www.scipiracicaba.com.br/imagens/logotipo-sci.png" />
<meta property="og:image:type" content="image/jpeg">
<!-- /Metas Facebook -->



<link rel="shortcut icon" href="http://www.scipiracicaba.com.br/favicon.ico" />

<link rel='shortcut icon' type='image/vnd.microsoft.icon' href='favicon.ico'> <!-- IE -->
<link rel='apple-touch-icon' type='image/png' href='icon.57.png'> <!-- iPhone -->
<link rel='apple-touch-icon' type='image/png' sizes='72x72' href='icon.72.png'> <!-- iPad -->
<link rel='apple-touch-icon' type='image/png' sizes='114x114' href='icon.114.png'> <!-- iPhone4 -->

<!-- CSS -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/estilo.css" type="text/css">

<!-- JQuery / Script -->
<script language="JavaScript" src="scripts/jquery-1.10.2.js"></script>
<script language="JavaScript" src="scripts/jquery-ui-1.10.4.custom.js"></script>
<script language="JavaScript" src="scripts/scripts.js"></script>



<!-- Fancy Box -->

<link type="text/css" href="fancy_box/css/jquery.fancybox.css" rel="stylesheet" />
<script type="text/javascript" src="fancy_box/js/jquery.fancybox.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {

	$(".fancybox").fancybox({
		openEffect	: "fade",
		closeEffect	: "fade"	
	});

});

$(document).ready(function() {
	$(".fancyboxc").fancybox({
	openEffect	: 'elastic',
	closeEffect	: 'elastic',
	scrolling : 'no'
	});
});

$(document).ready(function() {
	$(".various").fancybox({
		maxWidth	: 750,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '80%',
		scrolling: 'no',
		autoSize	: false,
		//beforeLoad: function () {$("body").css({ "overflow-y" : "scroll" });$("html").css({ "overflow-y" : "scroll" });},
		//afterClose: function () {$("body").css({ "overflow-y" : "scroll" });$("html").css({ "overflow-y" : "hidden" });},
		closeClick	: true,
		openEffect	: 'fade',
		closeEffect	: 'fade'
	});
});

$(document).ready(function() {
	$(".various2").fancybox({
		maxWidth	: 700,
		maxHeight	: 400,
		fitToView	: false,
		width		: '70%',
		height		: '80%',
		scrolling: 'no',
		autoSize	: true,
		//beforeLoad: function () {$("body").css({ "overflow-y" : "scroll" });$("html").css({ "overflow-y" : "scroll" });},
		//afterClose: function () {$("body").css({ "overflow-y" : "scroll" });$("html").css({ "overflow-y" : "hidden" });},
		closeClick	: true,
		openEffect	: 'fade',
		closeEffect	: 'fade'
	});
});

</script>
<!-- /Fancy Box -->

<meta name="google-site-verification" content="7D1h9yE0gU1WQGus10cWMxjSI4GlIOrwVs2VFZbpOz8" />

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68535761-1', 'auto');
  ga('send', 'pageview');

</script>


<!-- POPUP -->

<!-- //POPUP -->

</head>

<body onclick="efeito_fecha('div_menu')" onload="rolagemBloco(0,0)">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>


<a class="fancybox fancybox.iframe" id="popupclientes" href="pop_up_clientes.asp"><!-- pop-up-cliente --></a>




<div id="div-vazio" style="display:none"></div>

<table class="tabela-principal">
<tr><td valign="top">

	<table class="menu">
	<tr><td>
	
		<table class="tabela-conteudo" align="center" valign="top">
		<tr>
			<td align="left"><div style="float:left;margin-top:-4px"><a href=""><img src="imagens/logo-sci.jpg" width="400" height="113" border="0" alt="SCI - Sistema de Consumo Inteligente" title="SCI - Sistema de Consumo Inteligente" /></a></div></td>
			<td align="right">
			
				<table>
				<!-- Área restrita -->
				<tr><td align="right">
				
					
					<table>
					<tr><td colspan="5" align="left" class="textoGeral bold">ÁREA RESTRITA</td></tr>
					<tr><td colspan="5" height="5px" valign="top">
					
						<div style="position:relative">
						<div id="erro-ao-logar" class="div-erro-log">Os dados digitados estão incorretos</div>
						</div>
					
					</td></tr>
					<tr>
						<td><input type="text" name="lo-topo" id="lo-topo" class="input" style="width: 80px" placeholder="ID" /></td>
						<td style="width: 10px"></td>
						<td><input type="password" name="se-topo" id="se-topo" class="input" style="width: 80px" placeholder="Senha" /></td>
						<td style="width: 10px"></td>
						<td><input type="button" class="botao" value="Entrar" onclick="bannerImg()" /></td>
					</tr>
					<tr><td colspan="5" style="height: 3px" valign="top">

						<div class="relative">
						<div class="absolute" style="left:0px; top:5px;z-index:1000; display:none" id="esqueci-minha-senha">

							<table class="tabela-esqueci-senha">
							<tr><td style="height: 10px"></td></tr>
							<tr><td align="center">

								<div id="div-conteudo-esqueci-senha">
								<table>
								<tr><td>Digite seu CPF de cadastro</td></tr>
								<tr><td style="height: 5px"></td></tr>
								<tr><td><input type="text" name="cpf_esqueci" id="cpf_esqueci" class="input" onkeypress="formatar(this,'###.###.###-##');return SomenteNumero(event)" />&nbsp;&nbsp;<input type="button" value="Enviar" class="botao" onclick="esqueciSenha2()" /></td></tr>
								</table>
								</div>

							</td></tr>
							<tr><td style="height: 10px"></td></tr>
							</table>

						</div>
						</div>

					</td></tr>
					<tr><td colspan="5" align="left" class="esqueci-senha" ><span onclick="efeito_abre('esqueci-minha-senha')">Esqueci minha senha</span></td></tr>					</table>
					

				</td></tr>
				<tr><td style="height: 30px"></td></tr>
				<!-- Menu -->
				<tr><td valign="bottom" style="position:relative" height="19">
				
					<div style="position:absolute;right:0px;top:6px">
					<table width="685">
					<tr>
						<td><a href="empresa" class="menu-link">EMPRESA</a></td>
						<td style="width: 15px"></td>
						<td><a href="pontos-de-apoio" class="menu-link">PONTOS DE APOIO</a></td>
						<td style="width: 15px"></td>
						<td><a href="entregas-a-domicilio" class="menu-link">ENTREGAS A DOMICÍLIO</a></td>
						<td style="width: 15px"></td>
						<td><a href="blog" class="menu-link">BLOG OFICIAL</a></td>
						<td style="width: 15px"></td>
						<td><a href="cadastro" class="menu-link">CADASTRO</a></td>
						<td style="width: 15px"></td>
						<td><a href="contato" class="menu-link">CONTATO</a></td>
					</tr>
					</table>
					</div>
				
				</td></tr>
				</table>
			
			</td>
		</tr>
		</table>
	
	</td></tr>
	</table>

</td></tr>
<tr><td class="fundo-banner">

<style type="text/css">
.divTitulo {float:left;width:160px;height:33px;border:1px solid #e6e6e6;border-top:0px;border-bottom:0px;font-family:tahoma,verdana;font-size:12px;background:url('imagens/logotipo-sci.png') no-repeat 15px 6px #ffffff;padding-left:60px;padding-bottom:2px;padding-top:5px;color:#888888;text-align:left;}
.divBloco {position:relative;width:220px;height:240px;border:1px solid #e6e6e6;overflow:hidden;}
.bloco1 {position:absolute;top:0px;width:220px;font-family:tahoma;font-size:13px;background:#ffffff}
.bloco2 {position:absolute;top:252px;width:220px;font-family:tahoma;font-size:13px;background:#ffffff}
.bloco3 {position:absolute;top:252px;width:220px;font-family:tahoma;font-size:13px;background:#ffffff}
.cidade {font-family:tahoma,verdana;font-size:11px;text-transform:capitalize}
.nome {width:100%;float:left;text-align:left;font-family:tahoma,verdana;font-size:11px;height:36px;padding-left:8px;padding-top:5px;border-bottom:1px solid #e6e6e6;line-height:15px}

</style>

<script type="text/javascript">
function rolagemBloco(nome,altura){
$(".bloco1").animate({top:altura +"px"},500,function(){
nome = parseFloat(nome) + 1;
altura = parseFloat(altura) - 42;

	if (nome == 21){ //21
	} else if (nome == 14){ //14
	setTimeout("rolagemBloco("+ nome +","+ altura +")",1000);
	setTimeout("rolagemBloco2(0,252)",1000);
	} else if (nome == 1){ //14
	$(".bloco2").css({"top":"252px"});
	setTimeout("rolagemBloco("+ nome +","+ altura +")",1000);
	} else {
	setTimeout("rolagemBloco("+ nome +","+ altura +")",1000);
	}

});
}

///////// BLOCO 1 /////////////////////

function rolagemBloco1(nome,altura){
$(".bloco1").css({zIndex:"2"});
$(".bloco1").animate({top:altura +"px"},500,function(){
nome = parseFloat(nome) + 1;
altura = parseFloat(altura) - 42;

	if (nome == 21){ //14
	setTimeout("rolagemBloco2(1,210)",1000);
	setTimeout("rolagemBloco1("+ nome +","+ altura +")",1000);
	$(".bloco2").css({zIndex:"1"});
	} else if (nome == 1){ //14
	$(".bloco2").css({"top":"252px"});
	setTimeout("rolagemBloco1("+ nome +","+ altura +")",1000);
	} else if (nome == 27){ //14
	} else {
	setTimeout("rolagemBloco1("+ nome +","+ altura +")",1000);
	}

});
}

///////// BLOCO 2 /////////////////////

function rolagemBloco2(nome,altura){
$(".bloco2").css({zIndex:"2"});
$(".bloco2").animate({top:altura +"px"},500,function(){
nome = parseFloat(nome) + 1;
altura = parseFloat(altura) - 42;

	if (nome == 21){ //14
	setTimeout("rolagemBloco3(1,210)",1000);
	setTimeout("rolagemBloco2("+ nome +","+ altura +")",1000);
	$(".bloco3").css({zIndex:"1"});
	} else if (nome == 1){ //14
	$(".bloco3").css({"top":"252px"});
	setTimeout("rolagemBloco2("+ nome +","+ altura +")",1000);
	} else if (nome == 27){ //14
	} else {
	setTimeout("rolagemBloco2("+ nome +","+ altura +")",1000);
	}

});
}

///////// BLOCO 3 /////////////////////

function rolagemBloco3(nome,altura){
$(".bloco3").css({zIndex:"2"});
$(".bloco3").animate({top:altura +"px"},500,function(){
nome = parseFloat(nome) + 1;
altura = parseFloat(altura) - 42;

	if (nome == 21){ //14
	setTimeout("rolagemBloco1(1,210)",1000);
	setTimeout("rolagemBloco3("+ nome +","+ altura +")",1000);
	$(".bloco1").css({zIndex:"1"});
	} else if (nome == 1){ //14
	$(".bloco1").css({"top":"252px"});
	setTimeout("rolagemBloco3("+ nome +","+ altura +")",1000);
	} else if (nome == 27){ //14
	} else {
	setTimeout("rolagemBloco3("+ nome +","+ altura +")",1000);
	}

});
}
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


	<table class="tabela-conteudo" align="center">
	<tr>
	<td valign="top"><iframe src="banner2.asp" width="780" height="283" scrolling="no" frameborder="0"></iframe></td>
	<td valign="top" align="center" style="width:200px;height:283px;background:#fafafa">
		<table cellpadding="0" cellspacing="0" style="max-height:283px">
		<tr><td valign="top" align="center" style="max-height:283px;padding-right:3px">

			<div class="divTitulo"><b>A cada minuto um<br>novo cliente na SCI</b></div>
			<div class="divBloco">
					
			

				<div id="bloco1" class="bloco1"><div class="nome">FELIPE COSTA CORRÊA<br /><span class="cidade">Rio das Ostras/RJ</span></div><div class="nome">GRAZIELLA ALEXANDRA SOARES<br /><span class="cidade">Belo Horizonte/MG</span></div><div class="nome">MARCOS ROBSON DE AQUINO SOARES<br /><span class="cidade">Coroatá/MA</span></div><div class="nome">ROMILDO CARDOSO DA SILVA<br /><span class="cidade">Serra/ES</span></div><div class="nome">JENIFFER LUANA DA SILVA LEITE<br /><span class="cidade">Toledo/PR</span></div><div class="nome">SILMARA VIEIRA<br /><span class="cidade">Balneário Piçarras/SC</span></div><div class="nome">JOIRIAB FERREIRA DE ARAUJO<br /><span class="cidade">Lucas do Rio Verde/MT</span></div><div class="nome">REGIANE ALVES DA SILVA<br /><span class="cidade">Apucarana/PR</span></div><div class="nome">VIVIANE DA SILVEIRA<br /><span class="cidade">Pinhalzinho/SC</span></div><div class="nome">JOSE MARQUES DA SILVA NETO<br /><span class="cidade">Suzano/SP</span></div><div class="nome">MARIA CAROLINA DOS SANTOS BRAND...<br /><span class="cidade">Piraju/SP</span></div><div class="nome">MARLOS WILSON DALPIM<br /><span class="cidade">Poços de Caldas/MG</span></div><div class="nome">LINDOMAR GIOVANI GASDA JUNIOR<br /><span class="cidade">Massaranduba/SC</span></div><div class="nome">SILVONEI MANOEL BARBOSA ROCHA<br /><span class="cidade">Capelinha/MG</span></div><div class="nome">PAULO DE CASTRO PEREIRA<br /><span class="cidade">São José dos Pinhais/PR</span></div><div class="nome">GILMAR RODRIGUES MOURA <br /><span class="cidade">São Paulo/SP</span></div><div class="nome">EDSON ALVES DE SOUZA<br /><span class="cidade">Jandira/SP</span></div><div class="nome">ALAN CASTER RIBEIRO NASCIMENTO <br /><span class="cidade">Iturama/MG</span></div><div class="nome">MÁRCIA REGINA DE MELO<br /><span class="cidade">Belo Horizonte/MG</span></div><div class="nome">ALESSANDRO FERREIRA DE OLIVEIRA<br /><span class="cidade">Pelotas/RS</span></div></div>
				<div id="bloco2" class="bloco2"><div class="nome">FELIPE COSTA CORRÊA<br /><span class="cidade">Rio das Ostras/RJ</span></div><div class="nome">GRAZIELLA ALEXANDRA SOARES<br /><span class="cidade">Belo Horizonte/MG</span></div><div class="nome">MARCOS ROBSON DE AQUINO SOARES<br /><span class="cidade">Coroatá/MA</span></div><div class="nome">ROMILDO CARDOSO DA SILVA<br /><span class="cidade">Serra/ES</span></div><div class="nome">JENIFFER LUANA DA SILVA LEITE<br /><span class="cidade">Toledo/PR</span></div><div class="nome">SILMARA VIEIRA<br /><span class="cidade">Balneário Piçarras/SC</span></div><div class="nome">JOIRIAB FERREIRA DE ARAUJO<br /><span class="cidade">Lucas do Rio Verde/MT</span></div><div class="nome">REGIANE ALVES DA SILVA<br /><span class="cidade">Apucarana/PR</span></div><div class="nome">VIVIANE DA SILVEIRA<br /><span class="cidade">Pinhalzinho/SC</span></div><div class="nome">JOSE MARQUES DA SILVA NETO<br /><span class="cidade">Suzano/SP</span></div><div class="nome">MARIA CAROLINA DOS SANTOS BRAND...<br /><span class="cidade">Piraju/SP</span></div><div class="nome">MARLOS WILSON DALPIM<br /><span class="cidade">Poços de Caldas/MG</span></div><div class="nome">LINDOMAR GIOVANI GASDA JUNIOR<br /><span class="cidade">Massaranduba/SC</span></div><div class="nome">SILVONEI MANOEL BARBOSA ROCHA<br /><span class="cidade">Capelinha/MG</span></div><div class="nome">PAULO DE CASTRO PEREIRA<br /><span class="cidade">São José dos Pinhais/PR</span></div><div class="nome">GILMAR RODRIGUES MOURA <br /><span class="cidade">São Paulo/SP</span></div><div class="nome">EDSON ALVES DE SOUZA<br /><span class="cidade">Jandira/SP</span></div><div class="nome">ALAN CASTER RIBEIRO NASCIMENTO <br /><span class="cidade">Iturama/MG</span></div><div class="nome">MÁRCIA REGINA DE MELO<br /><span class="cidade">Belo Horizonte/MG</span></div><div class="nome">ALESSANDRO FERREIRA DE OLIVEIRA<br /><span class="cidade">Pelotas/RS</span></div></div>
				<div id="bloco3" class="bloco3"><div class="nome">FELIPE COSTA CORRÊA<br /><span class="cidade">Rio das Ostras/RJ</span></div><div class="nome">GRAZIELLA ALEXANDRA SOARES<br /><span class="cidade">Belo Horizonte/MG</span></div><div class="nome">MARCOS ROBSON DE AQUINO SOARES<br /><span class="cidade">Coroatá/MA</span></div><div class="nome">ROMILDO CARDOSO DA SILVA<br /><span class="cidade">Serra/ES</span></div><div class="nome">JENIFFER LUANA DA SILVA LEITE<br /><span class="cidade">Toledo/PR</span></div><div class="nome">SILMARA VIEIRA<br /><span class="cidade">Balneário Piçarras/SC</span></div><div class="nome">JOIRIAB FERREIRA DE ARAUJO<br /><span class="cidade">Lucas do Rio Verde/MT</span></div><div class="nome">REGIANE ALVES DA SILVA<br /><span class="cidade">Apucarana/PR</span></div><div class="nome">VIVIANE DA SILVEIRA<br /><span class="cidade">Pinhalzinho/SC</span></div><div class="nome">JOSE MARQUES DA SILVA NETO<br /><span class="cidade">Suzano/SP</span></div><div class="nome">MARIA CAROLINA DOS SANTOS BRAND...<br /><span class="cidade">Piraju/SP</span></div><div class="nome">MARLOS WILSON DALPIM<br /><span class="cidade">Poços de Caldas/MG</span></div><div class="nome">LINDOMAR GIOVANI GASDA JUNIOR<br /><span class="cidade">Massaranduba/SC</span></div><div class="nome">SILVONEI MANOEL BARBOSA ROCHA<br /><span class="cidade">Capelinha/MG</span></div><div class="nome">PAULO DE CASTRO PEREIRA<br /><span class="cidade">São José dos Pinhais/PR</span></div><div class="nome">GILMAR RODRIGUES MOURA <br /><span class="cidade">São Paulo/SP</span></div><div class="nome">EDSON ALVES DE SOUZA<br /><span class="cidade">Jandira/SP</span></div><div class="nome">ALAN CASTER RIBEIRO NASCIMENTO <br /><span class="cidade">Iturama/MG</span></div><div class="nome">MÁRCIA REGINA DE MELO<br /><span class="cidade">Belo Horizonte/MG</span></div><div class="nome">ALESSANDRO FERREIRA DE OLIVEIRA<br /><span class="cidade">Pelotas/RS</span></div></div>

			</div>

		</td></tr>
		</table>
	</td>
	</tr>
	</table>

</td></tr>
<tr><td valign="top"><div style="position:relative"><div class="sombra" style="position:absolute; left:0px; top:-283px"></div></div></td></tr>
<tr><td class="fundo-verde">
 
	<table class="tabela-conteudo" align="center">
	<tr><td style="height: 25px" valign="top"><img src="imagens/sombra-banner.png" width="980" height="23" border="0" alt="" /></td></tr>
	<tr><td align="center"><a href="https://pag.ae/bczzpD" target="_blank"><img src="imagens/gif_convenção.gif" width="1000" height="110" border="0" alt="Segunda convenção nacional da SCI" title="Segunda convenção nacional da SCI"></a></td></tr>
	<tr><td height="25"></td></tr>
	</table>

	<table class="tabela-conteudo" align="center">
	<tr><td class="titulo-home bold"><a href="blog" style="text-decoration:none; color:#25a585">ÚLTIMAS POSTAGENS</a></td></tr>
	<tr><td style="height: 10px"></td></tr>
	<tr><td>
	
		
		<!-- Blog home  -->
		<table class="tabela-conteudo">
		<tr>
			
			<td>
			
				<div class="DivBlogHome" onclick="window.open('blog/77/sci-participa-de-festa-tradicional-mineira-','_self')">
				<table style="width: 300px;">
				<tr><td style="height: 20px"></td></tr>
				<tr><td align="center">
					
					<table style="width: 250px">
					<tr><td style="background-image:url('img_site/blog/1662016117491.jpg');width: 100%; height: 100px; background-size:cover; background-position:center center "></td></tr>
					<tr><td style="height: 15px"></td></tr>
					<tr><td>
					
						<table>
						<tr>
							<td>
							
								<table>
								<tr><td align="center" class="textoGeral bold " style="font-size: 29px">16</td></tr>
								<tr><td class="textoGeral Fino" style="font-size: 12px">junho</td></tr>
								</table>
							
							</td>
							<td style="width: 10px"></td>
							<td class="texto-home bold" style="font-size: 13px" valign="top">SCI participa de festa tradicional Mineira </td>
						</tr>
						</table>
					
					</td></tr>
					<tr><td style="height: 8px"></td></tr>
					<tr><td style="height: 60px">A Minas Fest de Piracicaba faz parte do calendário oficial de eventos do município &#38nbsp;</td></tr>
					<tr><td style="height: 8px"></td></tr>
					<tr><td align="right"><a href="blog/77/sci-participa-de-festa-tradicional-mineira-" class="LinkLeiaMaisHome">leia mais</a></td></tr>
					</table>
				
				</td></tr>
				<tr><td style="height: 20px"></td></tr>
				</table>
				</div>
			
			</td>
			
			<td style="width: 20px"></td>
			
			<td>
			
				<div class="DivBlogHome" onclick="window.open('blog/73/capacitacao-em-alta-na-sci','_self')">
				<table style="width: 300px;">
				<tr><td style="height: 20px"></td></tr>
				<tr><td align="center">
					
					<table style="width: 250px">
					<tr><td style="background-image:url('img_site/blog/13520161112331.jpg');width: 100%; height: 100px; background-size:cover; background-position:center center "></td></tr>
					<tr><td style="height: 15px"></td></tr>
					<tr><td>
					
						<table>
						<tr>
							<td>
							
								<table>
								<tr><td align="center" class="textoGeral bold " style="font-size: 29px">12</td></tr>
								<tr><td class="textoGeral Fino" style="font-size: 12px">maio</td></tr>
								</table>
							
							</td>
							<td style="width: 10px"></td>
							<td class="texto-home bold" style="font-size: 13px" valign="top">Capacitação em Alta na SCI</td>
						</tr>
						</table>
					
					</td></tr>
					<tr><td style="height: 8px"></td></tr>
					<tr><td style="height: 60px">Agora são cinco os colaboradores que tem o curso de operador de Empilhadeira</td></tr>
					<tr><td style="height: 8px"></td></tr>
					<tr><td align="right"><a href="blog/73/capacitacao-em-alta-na-sci" class="LinkLeiaMaisHome">leia mais</a></td></tr>
					</table>
				
				</td></tr>
				<tr><td style="height: 20px"></td></tr>
				</table>
				</div>
			
			</td>
			
			<td style="width: 20px"></td>
			
			<td>
			
				<div class="DivBlogHome" onclick="window.open('blog/72/funcionario--do-mes-','_self')">
				<table style="width: 300px;">
				<tr><td style="height: 20px"></td></tr>
				<tr><td align="center">
					
					<table style="width: 250px">
					<tr><td style="background-image:url('img_site/blog/652016171231.jpg');width: 100%; height: 100px; background-size:cover; background-position:center center "></td></tr>
					<tr><td style="height: 15px"></td></tr>
					<tr><td>
					
						<table>
						<tr>
							<td>
							
								<table>
								<tr><td align="center" class="textoGeral bold " style="font-size: 29px">06</td></tr>
								<tr><td class="textoGeral Fino" style="font-size: 12px">maio</td></tr>
								</table>
							
							</td>
							<td style="width: 10px"></td>
							<td class="texto-home bold" style="font-size: 13px" valign="top">Funcionário  do Mês </td>
						</tr>
						</table>
					
					</td></tr>
					<tr><td style="height: 8px"></td></tr>
					<tr><td style="height: 60px">Colaborador da SCI é destaque e eleito o Funcionário do mês</td></tr>
					<tr><td style="height: 8px"></td></tr>
					<tr><td align="right"><a href="blog/72/funcionario--do-mes-" class="LinkLeiaMaisHome">leia mais</a></td></tr>
					</table>
				
				</td></tr>
				<tr><td style="height: 20px"></td></tr>
				</table>
				</div>
			
			</td>
			
		</tr>
		</table>
		
	
	</td></tr>
	</table>

	<table class="tabela-conteudo" align="center">
	<tr><td colspan="4" style="height: 30px"></td></tr>
	<tr>
		<td style="width:300px" valign="top">
		
			
			<table style="width: 100%">
			
			<tr><td><a href="depoimentos" class="titulo-home-link bold">DEPOIMENTOS</a></td></tr>
			<tr><td class="mini-texto-home fino">Após treinamento promovido pela Facilitadora Alexandra Ramos na cidade de Içara/SC, alguns líderes relataram suas experiências no aprendizado</td></tr>
			<tr><td style="height: 5px"></td></tr>
			<tr><td class="mini-texto-home fino"><b>Luiz - Içara-SC</b></td></tr>
			
			<tr><td style="height: 5px"></td></tr>
			<tr><td>
			<audio controls style="width: 300px">
			<source src="img_site/depoimentos/181120151049521.mp3" type="audio/mpeg" >
			</audio>
			</td></tr>
			
			<tr><td style="height: 15px"></td></tr>
			
			<!-- <tr><td style="background:#FFFFFF; width: 300px; height: 200px"><img src="img_site/publicidade/61020151142431.jpg" style="width: 300px; height: 200px; border:0px; cursor:pointer" onerror="this.src='img_site/publicidade/anuncie.jpg'" alt="Anuncie aqui" title="Anuncie aqui" onclick="window.open('contagem.asp?cod_publicidade=11','_blank')"/></td></tr> -->
			<tr><td>

				

				<div style="float:left;width:calc(100% - 42px);padding:20px;padding-top:15px;padding-bottom:15px;border:1px solid #d5d6d6;background-color:#FFFFFF">
					<div style="float:left;width:100%;font-family:Open Sans,arial;font-size:22px;color:#414141;font-weight:700;line-height:26px;text-align:center">ABRA SEU CORAÇÃO E<br>FAÇA UMA BOA AÇÃO</div>

					<div style="float:left;width:108px;margin-right:10px;background:url('imagens/caminhao_docao.png');height:86px;margin-top:22px"></div>
					<div style="float:left;width:calc(100% - 118px);text-align:center;font-family:Open Sans,arial;color:#9e302a;margin-top:20px">

						<div style="float:left;width:100%;font-size:34px;font-weight:700;line-height:19px">69<span style="font-size:22px">/300</span></div>
						<div style="float:left;width:100%;font-size:14px;margin-top:10px">pessoas já doaram<br>sua cesta</div>
						<div style="float:left;width:100%;color:#2244ed;font-size:12px;margin-top:7px"><a href="http://www.scipiracicaba.com.br/img_site/arquivos/cartilha-doacao.pdf" style="color:#2244ed;text-decoration:none" target="_blank">Clique aqui e saiba mais</a></div>
					
					</div>
				</div>
			
			</td></tr>
			<tr><td style="height: 20px"></td></tr>
			<tr><td>

				<div class="fb-page" data-href="https://www.facebook.com/SCIPiracicaba" data-width="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/SCIPiracicaba"><a href="https://www.facebook.com/SCIPiracicaba">SCI Piracicaba</a></blockquote></div></div>

			</td></tr>
			</table>
		
		</td>
		<td style="width: 10px; border-right: 4px dotted #25a585"></td>
		<td style="width: 10px"></td>
		<td style="width: 660px" valign="top">
		
			<table style="cursor:pointer; width: 660px" cellpadding="0" cellspacing="0">
			<tr><td colspan="3" style="padding-left: 30px; cursor:pointer" class="texto-home fino" onclick="window.open('empresa','_self')" align="left">Visamos formar uma carteira de clientes cadastrados (em todo território BRASILEIRO), que consumam produtos SCI e participem de todos os bônus gerados referente ao consumo de suas Equipes.</td></tr>
			<tr><td colspan="3" style="height: 20px"></td></tr>
			<tr onmouseover="EfeitoHomeMouse1('cadastro')" onmouseout="EfeitoHomeMouse2('cadastro')">
				<td align="center" onclick="window.open('cadastro','_self')"><img src="imagens/cadastro.png" width="57" height="70" border="0" alt="Cadastro" id="img_cadastro" /></td>
				<td style="width: 10px"></td>
				<td>
					
					<table cellpadding="0" cellspacing="0" onclick="window.open('cadastro','_self')">
					<tr><td class="titulo-home bold" id="titulo_cadastro">CADASTRE-SE</td></tr>
					<tr><td style="height: 10px"></td></tr>
					<td class="mini-texto-home fino" id="texto_cadastro">Escolha seu patrocinador, seu ponto de apoio e seja um divulgador</td>
					</table>
				
				</td>
			</tr>
			<tr><td colspan="3" style="height: 20px"></td></tr>
			
			<tr onmouseover="EfeitoHomeMouse1('apresentacao')" onmouseout="EfeitoHomeMouse2('apresentacao')">
				<td align="center" onclick="window.open('img_site/arquivos/6620161717151.pdf','_blank')"><img src="imagens/apresentacao.png" width="57" height="70" border="0" alt="Apresentação de Negócios" id="img_apresentacao" /></td>
				<td style="width: 10px"></td>
				<td>
					
					<table cellpadding="0" cellspacing="0"  onclick="window.open('img_site/arquivos/6620161717151.pdf','_blank')">
					<tr><td class="titulo-home bold" id="titulo_apresentacao">APRESENTAÇÃO DE NEGÓCIOS</td></tr>
					<tr><td style="height: 10px"></td></tr>
					<td class="mini-texto-home fino" id="texto_apresentacao">Entenda melhor como funciona a SCI de forma simples e direta!</td>
					</table>
				
				</td>
			</tr>
			<tr><td colspan="3" style="height: 20px"></td></tr>
			
			<tr onmouseover="EfeitoHomeMouse1('catalogo')" onmouseout="EfeitoHomeMouse2('catalogo')">
				<td align="center" onclick="window.open('img_site/arquivos/1462016105471.pdf','_blank')"><img src="imagens/catalogo.png" width="57" height="70" border="0" alt="Catálogo" id="img_catalogo" /></td>
				<td style="width: 10px"></td>
				<td>
					
					<table cellpadding="0" cellspacing="0"  onclick="window.open('img_site/arquivos/1462016105471.pdf','_blank')">
					<tr><td class="titulo-home bold" id="titulo_catalogo">CATÁLOGO</td></tr>
					<tr><td style="height: 10px"></td></tr>
					<td class="mini-texto-home fino" id="texto_catalogo">Conheça todos os produtos que a SCI pode te oferecer!</td>
					</table>
				
				</td>
			</tr>
			<tr><td colspan="3" style="height: 20px"></td></tr>
			

			<tr  onmouseover="EfeitoHomeMouse1('video-conferencia')" onmouseout="EfeitoHomeMouse2('video-conferencia')">
				<td align="center" onclick="window.open('http://login.hotconference.net.br/conference,scipiracicaba','_blank')"><img src="imagens/video-conferencia.png" width="57" height="70" border="0" alt="Vídeo Conferência" id="img_video-conferencia" /></td>
				<td style="width: 10px"></td>
				<td>
					
					
					<table cellpadding="0" cellspacing="0" onclick="window.open('http://login.hotconference.net.br/conference,scipiracicaba','_blank')">
					<tr><td class="titulo-home bold" id="titulo_video-conferencia">CONFERÊNCIA</td></tr>
					<tr><td style="height: 10px"></td></tr>
					<td class="mini-texto-home fino" id="texto_video-conferencia">Participe de nossa vídeo conferência, toda segunda e quinta às 20hs. Clique aqui</td>
					</table>
				
				</td>
			</tr>
			<tr><td colspan="3" style="height: 20px"></td></tr>
			<tr onmouseover="EfeitoHomeMouse1('ponto-apoio')" onmouseout="EfeitoHomeMouse2('ponto-apoio')">
				<td align="center" onclick="window.open('pontos-de-apoio','_self')"><img src="imagens/ponto-apoio.png" width="57" height="70" border="0" alt="Pontos de Apoio" id="img_ponto-apoio" /></td>
				<td style="width: 10px"></td>
				<td>
					
					<table cellpadding="0" cellspacing="0"  onclick="window.open('pontos-de-apoio','_self')">
					<tr><td class="titulo-home bold" id="titulo_ponto-apoio">PONTOS DE APOIO</td></tr>
					<tr><td style="height: 10px"></td></tr>
					<td class="mini-texto-home fino" id="texto_ponto-apoio">Escolha seu patrocinador, seu ponto de apoio e seja um divulgador</td>
					</table>
				
				</td>
			</tr>
			<tr><td colspan="3" style="height: 20px"></td></tr>
			<tr onmouseover="EfeitoHomeMouse1('calendario-reunioes')" onmouseout="EfeitoHomeMouse2('calendario-reunioes')">
				<td align="center" onclick="window.open('https://sharing.calendar.live.com/calendar/private/f5214e60-49b5-4cbb-b9d6-df07e64864cb/64651779-12ee-4569-84e5-e77f748d7795/cid-34202e22faa050a1/index.html','_blank')"><img src="imagens/calendario-reunioes.png" width="57" height="57" border="0" alt="Calendário de Reuniões" id="img_calendario-reunioes" /></td>
				<td style="width: 10px"></td>
				<td>
					
					<table cellpadding="0" cellspacing="0"  onclick="window.open('https://sharing.calendar.live.com/calendar/private/f5214e60-49b5-4cbb-b9d6-df07e64864cb/64651779-12ee-4569-84e5-e77f748d7795/cid-34202e22faa050a1/index.html','_blank')">
					<tr><td class="titulo-home bold" id="titulo_calendario-reunioes">CALENDÁRIO DE REUNIÕES</td></tr>
					<tr><td style="height: 10px"></td></tr>
					<td class="mini-texto-home fino" id="texto_calendario-reunioes">Confira nossa agenda, reuniões presenciais toda semana</td>
					</table>
				
				</td>
			</tr>
			</table>
		
		</td>
	</tr>
	<tr><td colspan="4" style="height: 60px"></td></tr>
	</table>
	

</td></tr>
<tr><td class="rodape">

	<table class="tabela-conteudo" align="center">
	<tr><td colspan="4" style="height: 30px"></td></tr>
	<tr>
		<td align="left" valign="top" style="width: 220px">
			
			<table>
			<tr><td class="telefone-rodape">Institucional</td></tr>
			<tr><td style="height: 10px"></td></tr>
			<tr><td><a href="empresa" class="linkRodape">Empresa</a></td></tr>
			<tr><td style="height: 3px"></td></tr>
			<tr><td><a href="blog" class="linkRodape">Blog</a></td></tr>
			<tr><td style="height: 3px"></td></tr>
			<tr><td><a href="cadastro" class="linkRodape">Cadastro</a></td></tr>
			<tr><td style="height: 3px"></td></tr>
			<tr><td><a href="contato" class="linkRodape">Contato</a></td></tr>
			<tr><td style="height: 3px"></td></tr>
			<tr><td><a href="pontos-de-apoio" class="linkRodape">Pontos de Apoio</a></td></tr>
			</table>
		
		</td>
		<td align="left" valign="top" style="width: 220px">

			<table>
			<tr><td class="telefone-rodape">Dúvidas?</td></tr>
			<tr><td style="height: 10px"></td></tr>
			<tr><td>(19) <font style="font-size: 18px">3415-5457</font></td></tr>
			<tr><td>(19) <font style="font-size: 18px">9 9488-1795</font> - Claro</td></tr>
			<tr><td>(19) <font style="font-size: 18px">9 8724-7031</font> - Oi</td></tr>
			<tr><td>(19) <font style="font-size: 18px">9 8241-3652</font> - Tim</td></tr>
			<tr><td>(19) <font style="font-size: 18px">9 9981-3100</font> - Vivo <!-- &nbsp;<img src="imagens/whats.png" width="20" height="20" border="0" alt="WhatsApp" align="center"/> --></td></tr>
			<tr><td style="height: 10px"></td></tr>
			<tr><td style="font-size: 16px"><strong>Horário de atendimento:<br>09h00 às 17h00</strong></td></tr>
			</table>
		
		</td>
		<td align="left" valign="top" >

			<table>
			<tr><td class="telefone-rodape">Localização</td></tr>
			<tr><td style="height: 10px"></td></tr>
			<tr><td>Rua Angelo Carregari, 33</td></tr>
			<tr><td>Resid Vale do Sol - Piracicaba, SP</td></tr>
			<tr><td>CEP: 13.406-001</td></tr>
			<tr><td style="height: 10px"></td></tr>
			<tr><td>
				
				<table>
				<tr>
					<td><a href="http://www.facebook.com/SCIPiracicaba" target="_blank"><img src="imagens/facebook.png" width="31" height="26" border="0" alt="Facebook" title="Facebook" /></a></td>
					<td style="width: 15px"></td>
					<td><a href="https://plus.google.com/110384316062578551070/about" target="_blank"><img src="imagens/google.png" width="31" height="26" border="0" alt="Google +" id="Google +" /></a></td>
				</tr>
				</table>
			
			</td></tr>
			</table>
		
		</td>

		<td align="right" style="width: 310px">

			<table>
			<tr><td>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3681.2742577200247!2d-47.70553548440177!3d-22.68083213555065!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c634605d5a4179%3A0xa92ca3aff5ea8e36!2sSCI+Piracicaba!5e0!3m2!1spt-BR!2sbr!4v1447238623134" width="300" height="200" frameborder="0" style="border:0" allowfullscreen></iframe></td></tr>
			</table>
		
		</td>
	</tr>
	<tr><td colspan="4" style="height:20px"></td></tr>
	</table>

</td></tr>
<tr><td>

<div style="width: 100%; bottom: 0px; left: 0px; z-index: 999; position: fixed;">
	<div style="width: 980px; margin: 0px auto; text-align: right; position: relative;">
		<div style="position: absolute; right: -120px; bottom: 60px;"><a href="http://www.scipiracicaba.com.br/convencao2016" target="_blank"><img src="imagens/bottom-convencao.png" border="0" alt="CONVENÇÃO 2016 - Garanta já seu passaporte com desconto" title="CONVENÇÃO 2016 - Garanta já seu passaporte com desconto" /></a></div>
	</div>
</div>

</td></tr>
</table>
  
</body>
</html>

